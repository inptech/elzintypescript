interface UserInterface {
        name: string;
        age?: number;
        getMessage(): string;
    }
    
    const user: UserInterface = {
        name: "Elzin",
        age: 19,
        getMessage() {
            return "Hello" + name;
        },
    };
    
    const user2: UserInterface = {
        name: "Kenan",
        getMessage() {
            return "Hello" + name;
        },
     };
     
     console.log(user.getMessage());